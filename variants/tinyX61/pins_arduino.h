/*
  pins_arduino.c - pin definitions for the Arduino board
  Part of Arduino / Wiring Lite

  Copyright (c) 2005 David A. Mellis

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General
  Public License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place, Suite 330,
  Boston, MA  02111-1307  USA
*/

#ifndef Pins_Arduino_h
#define Pins_Arduino_h

#include <avr/pgmspace.h>

// ATMEL ATtinyX61 / ARDUINO
//
//                  +-\/-+
//      (D 8)  PB0  1|    |20 PA0 (D 0) Ain0
//      (D 9)  PB1  2|    |19 PA1 (D 1) Ain1
//      (D 10) PB2  3|    |18 PA2 (D 2) Ain2
//      (D 11) PB3  4|    |17 PA3 (D 3)
//             VCC  5|    |16 AGND
//             GND  6|    |15 AVCC
// Ain7 (D 12) PB4  7|    |14 PA4 (D 4) Ain3
// Ain8 (D 13) PB5  8|    |13 PA5 (D 5) Ain4
// Ain9 (D 14) PB6  9|    |12 PA6 (D 6) Ain5
//      (D 15) PB7 10|    |11 PA7 (D 7) Ain6
//                   +----+

#define NUM_DIGITAL_PINS            16
#define NUM_ANALOG_INPUTS           10

#define analogInputToDigitalPin(p)  ((p < 3) ? (p) : ((p < 7) ? (p) + 1 : ((p < 10) ? (p) + 5 : -1)))

#define PIN_A0   (0)
#define PIN_A1   (1)
#define PIN_A2   (2)
#define PIN_A3   (4)
#define PIN_A4   (5)
#define PIN_A5   (6)
#define PIN_A6   (7)
#define PIN_A7   (12)
#define PIN_A8   (13)
#define PIN_A9   (14)

static const uint8_t A0 = PIN_A0;
static const uint8_t A1 = PIN_A1;
static const uint8_t A2 = PIN_A2;
static const uint8_t A3 = PIN_A3;
static const uint8_t A4 = PIN_A4;
static const uint8_t A5 = PIN_A5;
static const uint8_t A6 = PIN_A6;
static const uint8_t A7 = PIN_A7;
static const uint8_t A8 = PIN_A8;
static const uint8_t A9 = PIN_A9;

#define digitalPinToPCICR(p)    (((p) >= 0 && (p) <= 15) ? (&GIMSK) : ((uint8_t *)0))
#define digitalPinToPCICRbit(p) (((p) <= 7 || (p) >= 12) ? PCIE1 : PCIE0)
#define digitalPinToPCMSK(p)    (((p) <= 7) ? (&PCMSK0) : (((p) <= 15) ? (&PCMSK1) : ((uint8_t *)0)))
#define digitalPinToPCMSKbit(p) (((p) <= 7) ? (p) : ((p) - 8))

#define digitalPinToInterrupt(p)  ((p) == 14 ? 0 : ((p) == 2 ? 1 : NOT_AN_INTERRUPT))

//#define analogPinToChannel(p)   ( (p) < 6 ? (p) : (p) - 6 )

// Fix for SoftwareSerial (maybe other stuff too)
#define PCINT0_vect PCINT_vect

#ifdef ARDUINO_MAIN

/*
void initVariant() {
	GTCCR |= (1 << PWM1B);
}
*/

const uint16_t PROGMEM port_to_mode_PGM[] = {
	NOT_A_PORT,
	(uint16_t) &DDRA,
	(uint16_t) &DDRB,
};

const uint16_t PROGMEM port_to_output_PGM[] = {
	NOT_A_PORT,
	(uint16_t) &PORTA,
	(uint16_t) &PORTB,
};

const uint16_t PROGMEM port_to_input_PGM[] = {
	NOT_A_PIN,
	(uint16_t) &PINA,
	(uint16_t) &PINB,
};

const uint8_t PROGMEM digital_pin_to_port_PGM[] = {
	PA,
	PA,
	PA,
	PA,
	PA, 
	PA,
	PA,
	PA,

	PB,
	PB,
	PB,
	PB,
	PB,
	PB,
	PB,
	PB,
};

const uint8_t PROGMEM digital_pin_to_bit_mask_PGM[] = {
	_BV(0),
	_BV(1),
	_BV(2),
	_BV(3),
	_BV(4),
	_BV(5),
	_BV(6),
	_BV(7),

	_BV(0),
	_BV(1),
	_BV(2),
	_BV(3),
	_BV(4),
	_BV(5),
	_BV(6),
	_BV(7),
};

const uint8_t PROGMEM digital_pin_to_timer_PGM[] = {
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,

	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
	NOT_ON_TIMER,
};

#endif

#endif
